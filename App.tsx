import React, {useCallback, useEffect, useRef, useState} from 'react';
import {Pressable, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {
  Video,
  VideoFullscreenUpdate,
  VideoFullscreenUpdateEvent,
} from 'expo-av';

function App(): JSX.Element {
  const [isFullscreen, setIsFullscreen] = useState(false);
  const [isWorkaroundEnabled, setIsWorkaroundEnabled] = useState(false);
  const [isPlayerShown, setIsPlayerShown] = useState(true);
  const [presentCalls, setPresentCalls] = useState(0);
  const [dismissCalls, setDismissCalls] = useState(0);

  const videoElem = useRef<Video>(null);

  const onFullscreenUpdate = useCallback(
    (event: VideoFullscreenUpdateEvent) => {
      if (event.fullscreenUpdate === VideoFullscreenUpdate.PLAYER_DID_PRESENT) {
        setIsFullscreen(true);
      } else if (
        event.fullscreenUpdate === VideoFullscreenUpdate.PLAYER_DID_DISMISS
      ) {
        setIsFullscreen(false);
      }
    },
    [],
  );

  const forceDismiss = useCallback(() => {
    setDismissCalls(c => c + 1);
    videoElem.current?.dismissFullscreenPlayer().catch(console.error);
    setIsPlayerShown(false);
    setIsFullscreen(false);
  }, []);

  useEffect(() => {
    if (isFullscreen) {
      setTimeout(async () => {
        try {
          if (isWorkaroundEnabled) {
            console.log('Calling forceDismiss');
            forceDismiss();
          } else {
            console.log('Calling dismissFullscreenPlayer');
            setDismissCalls(c => c + 1);
            await videoElem.current?.dismissFullscreenPlayer();
          }
        } catch (e) {
          console.error(e);
        }
      }, 1500);
    }
  }, [forceDismiss, isFullscreen, isWorkaroundEnabled]);

  return (
    <SafeAreaView>
      <View style={styles.spacerLarge} />
      {isPlayerShown ? (
        <Video
          shouldPlay
          useNativeControls
          ref={videoElem}
          style={styles.video}
          source={{
            uri: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
          }}
          onFullscreenUpdate={onFullscreenUpdate}
        />
      ) : (
        <View style={styles.placeholder} />
      )}
      <View style={styles.spacerLarge} />
      <View style={styles.buttonGroupContainer}>
        <View>
          <Button
            text="Video.presentFullscreenPlayer()"
            onPress={async () => {
              try {
                console.log('Calling presentFullscreenPlayer');
                setPresentCalls(c => c + 1);
                videoElem.current?.presentFullscreenPlayer();
              } catch (e) {
                console.error(e);
              }
            }}
          />
          <View style={styles.spacerMedium} />
          <Button
            text={
              isWorkaroundEnabled ? 'Disable workaround' : 'Enable workaround'
            }
            onPress={() => setIsWorkaroundEnabled(enabled => !enabled)}
          />
          <View style={styles.spacerMedium} />
          <Button
            text={isPlayerShown ? 'Unmount <Video />' : 'Remount <Video />'}
            onPress={() => setIsPlayerShown(shown => !shown)}
          />
          <View style={styles.spacerMedium} />
          <Text>presentFullscreenPlayer calls: {presentCalls}</Text>
          <View style={styles.spacerMedium} />
          <Text>dismissFullscreenPlayer calls: {dismissCalls}</Text>
        </View>
      </View>
    </SafeAreaView>
  );
}

function Button({onPress, text}: {onPress: () => void; text: string}) {
  return (
    <Pressable style={styles.button} onPress={onPress}>
      <Text style={styles.buttonText}>{text}</Text>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  video: {
    height: 175,
    width: '100%',
  },
  placeholder: {
    height: 175,
    width: '100%',
    backgroundColor: 'black',
  },
  spacerLarge: {
    height: 36,
  },
  spacerMedium: {
    height: 16,
  },
  buttonGroupContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    height: 40,
    width: 285,
    borderRadius: 5,
    backgroundColor: 'gray',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {color: 'white', fontWeight: 'bold'},
});

export default App;
