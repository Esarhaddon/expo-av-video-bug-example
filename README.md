This is a new [**React Native**](https://reactnative.dev) project, bootstrapped using [`@react-native-community/cli`](https://github.com/react-native-community/cli) for the purpose of demonstrating a bug with the `expo-av` `<Video />` element seen on an iPhone Xs running iOS 16.5.1. See the steps in the Running This Project Locally section for how to run this project locally.

# The Bug

On an iPhone Xs running iOS 16.5.1, calls to `dismissFullscreenPlayer` don't work unless preceded by calls to `presentFullscreenPlayer`. Haven't verified that it happens on other iOS devices, but it looks to me like [this issue](https://github.com/expo/expo/issues/10473) is related, so this doesn't seem to be specific to my exact configuration. This hasn't been a problem for me on Android.

## Expected Behavior

Calling `dismissFullscreenPlayer` should always close the fullscreen player.

## Steps To Reproduce

1. Tap the video to reveal the fullscreen icon.
1. Tap the fullscreen icon to show the fullscreen video.
1. Wait a couple seconds.
1. The fullscreen video should have been closed by the call to `dismissFullscreenPlayer` in the timeout after 1.5 seconds, but instead nothing happens.
1. If you tap the video again to show the close button and manually close the fullscreen video, you can verify that `dismissFullscreenPlayer` was called in the timeout by looking at the counter.

However, if you open the fullscreen player by pressing the first button underneath the video, which calls `presentFullscreenPlayer` directly, then the next call to `dismissFullscreenPlayer` in the timeout will work (you may have to manually dismiss the fullscreen player one more time before the call to `dismissFullscreenPlayer` will work).

Here's what the example should look like when run locally:

![IMG_477C8DE3F7D8-1](https://github.com/expo/expo/assets/51306065/31bd2695-988e-49c4-a8a6-7515bfea0ab4)

## My Workaround

My way of hacking around this is to unmount the video player after calling `dismissFullscreenPlayer`. This is a little clunky but seems to be adequate, although I'm wary of using it production. You can see for yourself by pressing the second button beneath the video (labeled "Enable Workaround"), and then repeating the steps above.

# Running This Project Locally

> **Note**: Make sure you have completed the [React Native - Environment Setup](https://reactnative.dev/docs/environment-setup) instructions till "Creating a new application" step, before proceeding.

## Step 1: Start the Metro Server

First, you will need to start **Metro**, the JavaScript _bundler_ that ships _with_ React Native.

To start Metro, run the following command from the _root_ of your React Native project:

```bash
# using npm
npm start

# OR using Yarn
yarn start
```

## Step 2: Start your Application

Let Metro Bundler run in its _own_ terminal. Open a _new_ terminal from the _root_ of your React Native project. Run the following command to start your _Android_ or _iOS_ app:

### For Android

```bash
# using npm
npm run android

# OR using Yarn
yarn android
```

### For iOS

```bash
# using npm
npm run ios

# OR using Yarn
yarn ios
```

If everything is set up _correctly_, you should see your new app running in your _Android Emulator_ or _iOS Simulator_ shortly provided you have set up your emulator/simulator correctly.

This is one way to run your app — you can also run it directly from within Android Studio and Xcode respectively.

## Step 3: Modifying your App

Now that you have successfully run the app, let's modify it.

1. Open `App.tsx` in your text editor of choice and edit some lines.
2. For **Android**: Press the <kbd>R</kbd> key twice or select **"Reload"** from the **Developer Menu** (<kbd>Ctrl</kbd> + <kbd>M</kbd> (on Window and Linux) or <kbd>Cmd ⌘</kbd> + <kbd>M</kbd> (on macOS)) to see your changes!

   For **iOS**: Hit <kbd>Cmd ⌘</kbd> + <kbd>R</kbd> in your iOS Simulator to reload the app and see your changes!

## Congratulations! :tada:

You've successfully run and modified your React Native App. :partying_face:

### Now what?

- If you want to add this new React Native code to an existing application, check out the [Integration guide](https://reactnative.dev/docs/integration-with-existing-apps).
- If you're curious to learn more about React Native, check out the [Introduction to React Native](https://reactnative.dev/docs/getting-started).

# Troubleshooting

If you can't get this to work, see the [Troubleshooting](https://reactnative.dev/docs/troubleshooting) page.

# Learn More

To learn more about React Native, take a look at the following resources:

- [React Native Website](https://reactnative.dev) - learn more about React Native.
- [Getting Started](https://reactnative.dev/docs/environment-setup) - an **overview** of React Native and how setup your environment.
- [Learn the Basics](https://reactnative.dev/docs/getting-started) - a **guided tour** of the React Native **basics**.
- [Blog](https://reactnative.dev/blog) - read the latest official React Native **Blog** posts.
- [`@facebook/react-native`](https://github.com/facebook/react-native) - the Open Source; GitHub **repository** for React Native.
